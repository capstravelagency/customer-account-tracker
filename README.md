# PreRequisite-Assignment

Developer 1: prasad.durga1@wipro.com [Prasad Machu Nagendra]

Developer 2 : prabhu.kencharaddi@wipro.com [Prabhu Kencharaddi]


This project contains the source code for PreRequisite-Assignment project. The App has been made using following technologies

`Spring boot` -  Controller

`Spring boot thymeleaf` -  UI

`webjars,bootstrap` - UI


## Configurations

 ``` application.properties ```
