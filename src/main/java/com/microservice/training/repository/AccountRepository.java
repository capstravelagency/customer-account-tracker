package com.microservice.training.repository;

import com.microservice.training.bean.Account;
 import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AccountRepository extends CrudRepository<Account, Integer> {

  Optional<Account> findByAccountId(int accountId);
}
